package example

object Hello
  extends App {

  def fizzbuzz(input: Int): String = input match {
    case _ if canModuloWith(input, 3) && canModuloWith(input, 5) => "FizzBuzz"
    case _ if canModuloWith(input, 3) => "Fizz"
    case _ if canModuloWith(input, 5) => "Buzz"
    case _ => input.toString
  }

  def canModuloWith(input: Int, mod: Int) =
    input % mod == 0

}
