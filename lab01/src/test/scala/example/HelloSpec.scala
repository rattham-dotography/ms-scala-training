package example

import org.scalatest._

class HelloSpec
  extends FlatSpec
    with Matchers {

  "input 1 in fizzbuzz" should
    "return 1" in {
    Hello.fizzbuzz(1) shouldEqual "1"
  }

  "input 2 in fizzbuzz" should
    "return 2" in {
    Hello.fizzbuzz(2) shouldEqual "2"
  }

  "input 3 in fizzbuzz" should
    "return Fizz" in {
    Hello.fizzbuzz(3) shouldEqual "Fizz"
  }

  "input 4 in fizzbuzz" should
    "return 4" in {
    Hello.fizzbuzz(4) shouldEqual "4"
  }

  "input 5 in fizzbuzz" should
    "return Buzz" in {
    Hello.fizzbuzz(5) shouldEqual "Buzz"
  }

  "input 6 in fizzbuzz" should
    "return Fizz" in {
    Hello.fizzbuzz(6) shouldEqual "Fizz"
  }

  "input 7 in fizzbuzz" should
    "return 7" in {
    Hello.fizzbuzz(7) shouldEqual "7"
  }

  "input 8 in fizzbuzz" should
    "return 8" in {
    Hello.fizzbuzz(8) shouldEqual "8"
  }

  "input 9 in fizzbuzz" should
    "return Fizz" in {
    Hello.fizzbuzz(9) shouldEqual "Fizz"
  }

  "input 10 in fizzbuzz" should
    "return Buzz" in {
    Hello.fizzbuzz(10) shouldEqual "Buzz"
  }

  "input 11 in fizzbuzz" should
    "return 11" in {
    Hello.fizzbuzz(11) shouldEqual "11"
  }

  "input 12 in fizzbuzz" should
    "return 12" in {
    Hello.fizzbuzz(12) shouldEqual "Fizz"
  }

  "input 13 in fizzbuzz" should
    "return 13" in {
    Hello.fizzbuzz(13) shouldEqual "13"
  }

  "input 14 in fizzbuzz" should
    "return 14" in {
    Hello.fizzbuzz(14) shouldEqual "14"
  }

  "input 15 in fizzbuzz" should
    "return FizzBuzz" in {
    Hello.fizzbuzz(15) shouldEqual "FizzBuzz"
  }

  "input 30 in fizzbuzz" should
    "return FizzBuzz" in {
    Hello.fizzbuzz(30) shouldEqual "FizzBuzz"
  }


//  "Fizzbuzz #2" should
//    "input 2 then return 2" in {
//    Hello.fizzbuzz(2) shouldEqual 2
//  }
//
//  "Fizzbuzz #3" should
//    "input 3 then return Fizz" in {
//    Hello.fizzbuzz(3) shouldEqual "Fizz"
//  }
//
//  "Fizzbuzz #4" should
//    "input 3 then return Fizz" in {
//    Hello.fizzbuzz(3) shouldEqual "Fizz"
//  }
}
