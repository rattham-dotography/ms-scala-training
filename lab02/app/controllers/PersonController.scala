package controllers

import javax.inject._

import forms.CreatePersonForm
import play.api.libs.json._
import play.api.mvc._
import presenters.Person
import facades.PersonFacade

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class PersonController @Inject()(cc: ControllerComponents,
                                 facade: PersonFacade)
  extends AbstractController(cc) {

  def find(id: Int) = Action { implicit request: Request[AnyContent] =>
    Ok(Json.toJson(facade.find(id)))
  }

  def list() = Action { implicit request: Request[AnyContent] =>
    Ok(Json.toJson(facade.list))
  }

  def create() = Action { implicit request: Request[AnyContent] =>
    CreatePersonForm.form.bindFromRequest.fold(
      formWithErros => InternalServerError("Invalid Input.")
      , form => try {
        if(facade.create(form))
          Ok(Json.toJson(Person(form)))
        else
          InternalServerError("Unable to Process Request.")
      } catch {
        case t: Throwable =>
          InternalServerError("Unable to Process Request.")
      }
    )
  }

}