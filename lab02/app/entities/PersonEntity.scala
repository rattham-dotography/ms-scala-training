package entities

import forms.CreatePersonForm

/**
  * Created by seriouspipe on 7/21/17.
  */
case class PersonEntity(id: Int, name: String)

object PersonEntity {

  def apply(form: CreatePersonForm): PersonEntity =
    PersonEntity(
      id = form.id
      , name = form.name)
}
