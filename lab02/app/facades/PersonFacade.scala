package facades

import javax.inject.{Inject, Singleton}

import entities.PersonEntity
import forms.CreatePersonForm
import persists.PersonPersist
import presenters.Person

/**
  * Created by seriouspipe on 7/21/17.
  */
@Singleton
class PersonFacade @Inject()(persist: PersonPersist) {

  def list: List[Person] =
    persist.list().map(entity => Person(entity))

  def find(id: Int): Option[Person] =
    persist.find(id).map(entity => Person(entity))

  def create(form: CreatePersonForm): Boolean = {
    persist.insert(PersonEntity(form))
  }
}
