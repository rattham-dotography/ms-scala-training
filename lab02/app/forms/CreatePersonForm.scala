package forms

import play.api.data.Form
import play.api.data.Forms.{mapping, of}
import play.api.data.format.Formats._

/**
  * Created by seriouspipe on 7/21/17.
  */
case class CreatePersonForm(id: Int, name: String)

object CreatePersonForm {

  val form = Form(
    mapping(
      "id" -> of[Int]
      , "first_name" -> of[String]
    )(CreatePersonForm.apply)(CreatePersonForm.unapply)
  )
}
