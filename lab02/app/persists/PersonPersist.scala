package persists

import java.sql.ResultSet
import javax.inject.{Inject, Singleton}

import entities.PersonEntity
import play.api.db.Database

/**
  * Created by seriouspipe on 7/21/17.
  */

@Singleton
class PersonPersist @Inject()(db: Database){

  def list(): List[PersonEntity] = db.withConnection { implicit conn =>
    val preparedStatement = conn.prepareStatement(LIST)
    val resultSet = preparedStatement.executeQuery()

    new Iterator[PersonEntity] {
      override def hasNext = resultSet.next()
      override def next = parse(resultSet)
    }.toList

  }

  def find(id: Int): Option[PersonEntity] = db.withConnection { implicit conn =>
    val preparedStatement = conn.prepareStatement(FIND)
    preparedStatement.setInt(1, id)

    val resultSet = preparedStatement.executeQuery()
    resultSet.next() match {
      case true => Some(parse(resultSet))
      case false => None
    }
  }

  def insert(entity: PersonEntity): Boolean = db.withConnection { implicit conn =>
    val preparedStatement = conn.prepareStatement(INSERT)
    preparedStatement.setInt(1, entity.id)
    preparedStatement.setString(2, entity.name)

    preparedStatement.executeUpdate() match {
      case 1 => true
      case _ => false
    }
  }

  private def parse(rs: ResultSet): PersonEntity =
    PersonEntity(
      id = rs.getInt("id"),
      name = rs.getString("name")
    )

  private lazy val LIST =
    """
      |SELECT *
      |FROM people
    """.stripMargin

  private lazy val FIND =
    """
      |SELECT *
      |FROM people
      |WHERE id = ?
    """.stripMargin

  private lazy val INSERT =
    """
      |INSERT INTO people(id, name)
      |VALUES(?,?)
    """.stripMargin

}
