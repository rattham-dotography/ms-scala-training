package presenters

import entities.PersonEntity
import forms.CreatePersonForm
import play.api.libs.json.{JsValue, Json, Writes}

/**
  * Created by seriouspipe on 7/21/17.
  */

case class Person(id: Int, name: String)

object Person {

  implicit val writes = new Writes[Person] {
    def writes(person: Person): JsValue = {
      Json.obj(
        "id" -> person.id,
        "first_name" -> person.name
      )
    }
  }

  def apply(form: CreatePersonForm): Person =
    Person(
      id = form.id
      , name = form.name
    )

  def apply(entity: PersonEntity): Person =
    Person(
      id = entity.id
      , name = entity.name
    )
}

