DROP TABLE IF EXISTS people;

CREATE TABLE people
(
    id      INT NOT NULL,
    name    VARCHAR(200) NOT NULL,
    CONSTRAINT people_pk PRIMARY KEY (id)
);

ALTER TABLE people OWNER TO lab03;