package controllers

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

/**
  * Add your spec here.
  * You can mock out a whole application including requests, plugins etc.
  *
  * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
  */
class PersonControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "GET /v1/people" should {

    "return list of all people" in {
      val request = FakeRequest(GET, "/v1/people")
      val home = route(app, request).get

      status(home) mustBe OK
      contentType(home) mustBe Some("application/json")
      contentAsString(home) must include ("rattham")
    }

  }

  "GET /v1/people/:id" should {

    "return person by id" in {
      val request = FakeRequest(GET, "/v1/people/2")
      val home = route(app, request).get

      status(home) mustBe OK
      contentType(home) mustBe Some("application/json")
      contentAsString(home) must include ("natha")
    }
  }

  "POST /v1/people" should {
    "return person whom created" in {
      val request = FakeRequest(POST, "/v1/people")
        .withJsonBody(Json.parse(
          """
            |{
            |"id" : 0,
            |"first_name" : "rattham"
            |}
            |""".stripMargin)
        )
      val home = route(app, request).get

      status(home) mustBe OK
      contentType(home) mustBe Some("application/json")
      contentAsString(home) must include("rattham")
    }
  }



}
