package persists

import entities.PersonEntity
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.test.Injecting

/**
  * Created by seriouspipe on 7/21/17.
  */
class PersonPersistSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "PersonPersist insert" should {
    "return true if success" in {
      val persist = inject[PersonPersist]
      assert(persist.insert(PersonEntity(1, "rattham")))
    }
  }
}
